import React, { Component } from "react";
import Form from "./Form";
import "./Body.css";

class Body extends Component {
  state = {
    class: ["form"]
  };

  HandleAddClass = () => {
    if (document.querySelector(".square").contains("active")) {
      this.setState({
        class: ["form"]
      });
    } else
      this.setState({
        class: ["form", "active"]
      });
  };
  render() {
    if (this.props.projects) {
      return (
        <div>
          <h1>PROJECTS</h1>
        </div>
      );
    } else if (this.props.about) {
      return (
        <div>
          <h1>ABOUT</h1>
        </div>
      );
    } else if (this.props.contact) {
      return (
        <div>
          <h1 className="contactTitle">CONTACT</h1>
          <div className="contactData">
            <p>e-mail: michal.ciombor@gmail.com</p>
            <p>mobile: +48 508 206 403</p>

            <ul>
              <li>
                <strong>blababla</strong>
              </li>
            </ul>
          </div>

          <Form onClick={this.HandleAddClass} active={this.state.class} />
        </div>
      );
    } else if (this.props.skills) {
      return (
        <div>
          <h1>SKILLS</h1>
        </div>
      );
    }
  }
}

export default Body;
