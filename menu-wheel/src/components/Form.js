import React, { Component } from "react";
import "./Form.css";

class Form extends Component {
  state = {
    user: "",
    email: "",
    pass: "",
    consent: false,
    class: ["square"],
    msg: "",

    errors: {
      user: false,
      email: false,
      pass: false,
      consent: false
    }
  };
  componentDidMount() {
    this.props.onClick.bind("form", "active");
  }

  messages = {
    username_incorrect: "Nazwa musi być dłuższa niż 5 znaków",
    email_incoreect: "Brak @ w adresie",
    password_incorrect: "Wiadomość musi mieć minimum 5 znaków",
    consent_incorrect: "Zgoda nie została potwierdzona"
  };

  handleChange = e => {
    const name = e.target.id;
    if (e.target.type !== "checkbox") {
      this.setState({
        [name]: e.target.value
      });
    } else {
      this.setState({
        [name]: !this.state.consent
      });
    }
  };
  handleSubmit = e => {
    e.preventDefault();
    const validation = this.formValidation();
    // console.log(validation)
    if (validation.correct) {
      //       <?php
      // $to      = 'nobody@example.com';
      // $subject = 'the subject';
      // $message = 'hello';
      // $headers = 'From: webmaster@example.com' . "\r\n" .
      //     'Reply-To: webmaster@example.com' . "\r\n" .
      //     'X-Mailer: PHP/' . phpversion();

      // mail($to, $subject, $message, $headers);
      // ?>
      this.setState({
        user: "",
        email: "",
        pass: "",
        consent: false,
        message: "Formularz został wysłany",
        class: ["square active"],
        msg: "THANK YOU! ",

        errors: {
          user: false,
          email: false,
          pass: false,
          consent: false
        }
      });
    } else {
      this.setState({
        msg: "PLEASE, CHECK AGAIN IF YOU FILL THE FORM CORRECTLY",
        class: ["square failure"],
        errors: {
          user: !validation.user,
          email: !validation.email,
          pass: !validation.pass,
          consent: !validation.consent
        }
      });
    }
  };
  formValidation = () => {
    let user = false;
    let email = false;
    let pass = false;
    let consent = false;
    let correct = false;

    if (this.state.user.length > 5 && this.state.user.indexOf(" ") === -1) {
      user = true;
    }
    if (this.state.email.indexOf("@") !== -1) {
      email = true;
    }
    if (this.state.pass.length > 4) {
      pass = true;
    }
    if (this.state.consent) {
      consent = true;
    }
    if (user && email && pass && consent) {
      correct = true;
    }
    return {
      user,
      email,
      pass,
      consent,
      correct
    };
  };
  back = () => {
    window.history.go(2);
  };
  componentDidUpdate() {
    if (this.state.message !== "") {
      setTimeout(() => {
        this.setState({
          message: ""
        });
      }, 3000);
      console.log("update");
    }
  }
  handleClick = () => {
    this.setState({
      class: ["square"]
    });
    this.back();
  };

  render() {
    return (
      <div className="form" id="form">
        {/* <div className={`${this.state.class}`}>OK!</div> */}
        <h2>Zostaw wiadomość:</h2>
        <form onSubmit={this.handleSubmit} noValidate>
          {this.state.class === ["square active"] ? (
            <div className={`${this.state.class}`}>{this.state.msg}</div>
          ) : (
            <div onClick={this.handleClick} className={`${this.state.class}`}>
              {this.state.msg}
            </div>
          )}
          <label htmlFor="user">
            Twoje imię:{" "}
            <input
              type="text"
              id="user"
              placeholder="Imię"
              value={this.state.user}
              onChange={this.handleChange}
            />
            {this.state.errors.user && (
              <span>{this.messages.username_incorrect}</span>
            )}
          </label>
          <label htmlFor="email">
            Twój e-mail:
            <input
              type="email"
              id="email"
              placeholder="@mail"
              value={this.state.email}
              onChange={this.handleChange}
            />
            {this.state.errors.email && (
              <span>{this.messages.email_incoreect}</span>
            )}
          </label>
          <label htmlFor="pass">
            Twoja wiadomość:
            <textarea
              rows="4"
              cols="40"
              type="textarea"
              id="pass"
              placeholder="Wiadomość"
              value={this.state.pass}
              onChange={this.handleChange}
            />
            {this.state.errors.pass && (
              <span>{this.messages.password_incorrect}</span>
            )}
          </label>
          <label htmlFor="consent">
            <input
              type="checkbox"
              id="consent"
              checked={this.state.consent}
              onChange={this.handleChange}
              value={this.state.consent}
            />{" "}
            Wyrażam zgodę na przechowywanie moich danych
            {this.state.errors.consent && (
              <span>{this.messages.consent_incorrect}</span>
            )}
          </label>
          <input
            type="submit"
            value="Wyślij formularz"
            onClick={this.handleSubmit}
          />
        </form>
        {this.state.message && <h3>{this.state.message}</h3>}
      </div>
    );
  }
}

export default Form;
