import React, { Component } from "react";
import Menu from "./Menu.js";
import Body from "./Body.js";

class App extends Component {
  state = {
    projects: false,
    skills: false,
    contact: false,
    about: true,
    rotation: 90
  };
  handleRotate = e => {
    console.log(e.target.id);
    if (e.target.id === "about") {
      this.setState({
        rotation: 90,
        projects: false,
        skills: false,
        contact: false,
        about: true
      });
    } else if (e.target.id === "contact") {
      this.setState({
        rotation: 180,
        projects: false,
        skills: false,
        contact: true,
        about: false
      });
    } else if (e.target.id === "skills") {
      this.setState({
        rotation: 270,
        projects: false,
        skills: true,
        contact: false,
        about: false
      });
    } else if (e.target.id === "projects") {
      this.setState({
        rotation: 0,
        projects: true,
        skills: false,
        contact: false,
        about: false
      });
    }
  };
  render() {
    return (
      <div>
        <Menu
          onClick={() => this.handleRotate}
          rotation={this.state.rotation}
          projects={this.state.projects}
          about={this.state.about}
          skills={this.state.skills}
          contact={this.state.contact}
        />
        <Body
          projects={this.state.projects}
          about={this.state.about}
          skills={this.state.skills}
          contact={this.state.contact}
          
        />
      </div>
    );
  }
}

export default App;
