import React, { Component } from "react";
import { NavLink, BrowserRouter as Router } from "react-router-dom";
import "./Menu.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faUserAstronaut,
  faProjectDiagram,
  faHandsHelping,
  faDiagnoses
} from "@fortawesome/free-solid-svg-icons";

class Menu extends Component {
  state = {};

  render() {
    return (
      <Router>
        <div className="wrapper">
          <div
            className="mainCircle"
            style={{ transform: `rotate(${this.props.rotation}deg)` }}
          >
            <NavLink
              to="/projects"
              id="projects"
              onClick={this.props.onClick("projects")}
              className="projects"
              activeClassName="nav__item--active"
            >
              PROJECTS
            </NavLink>
            <NavLink
              to="/contact"
              id="contact"
              onClick={this.props.onClick("contact")}
              className="contact"
              activeClassName="nav__item--active"
            >
              CONTACT
            </NavLink>
            <NavLink
              to="/skills"
              id="skills"
              onClick={this.props.onClick("skills")}
              className="skills"
              activeClassName="nav__item--active"
            >
              SKILLS
            </NavLink>
            <NavLink
              to="/about"
              id="about"
              onClick={this.props.onClick("about")}
              className="about"
              activeClassName="nav__item--active"
            >
              ABOUT
            </NavLink>
            <div className="contactIcon">
              {this.props.contact ? (
                <FontAwesomeIcon
                  icon={faHandsHelping}
                  style={{ transform: "rotate(180deg)" }}
                />
              ) : (
                ""
              )}
              {this.props.skills ? (
                <FontAwesomeIcon
                  icon={faDiagnoses}
                  style={{ transform: "rotate(90deg)" }}
                />
              ) : (
                ""
              )}
              {this.props.about ? (
                <FontAwesomeIcon
                  icon={faUserAstronaut}
                  style={{ transform: "rotate(-90deg)" }}
                />
              ) : (
                ""
              )}
              {this.props.projects ? (
                <FontAwesomeIcon icon={faProjectDiagram} />
              ) : (
                ""
              )}
            </div>
          </div>
        </div>
      </Router>
    );
  }
}

export default Menu;
