const gulp = require("gulp");
const sass = require("sass");

gulp.task("sass", function() {
  return gulp
    .src("./src/components/sass/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("./css"));
});
